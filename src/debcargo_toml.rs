//! This is currently an incomplete write-only representation of the
//! `debcargo.toml` file structure. Don't expect to be able to load and
//! write the file and have the same structure.

use config;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};
use toml;
use Result;

fn value(b: &bool) -> bool {
    *b
}
fn not_value(b: &bool) -> bool {
    !value(b)
}
fn get_false() -> bool {
    false
}
fn get_true() -> bool {
    true
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Debcargo {
    #[serde(default)]
    pub overlay: Option<PathBuf>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub uploaders: Vec<String>,
    #[serde(default = "get_false", skip_serializing_if = "not_value")]
    pub semver_suffix: bool,
    #[serde(default = "get_true", skip_serializing_if = "value")]
    pub bin: bool,
    pub bin_name: Option<String>,
    pub summary: Option<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub excludes: Vec<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub whitelist: Vec<String>,
    #[serde(default = "get_false", skip_serializing_if = "not_value")]
    pub allow_prerelease_deps: bool,
    pub source: Option<Source>,
    #[serde(default, skip_serializing_if = "BTreeMap::is_empty")]
    pub packages: BTreeMap<String, Bin>,
}

#[derive(Default, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Source {
    pub homepage: Option<String>,
    pub vcs_git: Option<String>,
    pub vcs_browser: Option<String>,
    pub policy: Option<String>,
    pub section: Option<String>,
    pub build_depends: Option<Vec<String>>,
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Bin {
    pub section: Option<String>,
    pub summary: Option<String>,
    pub description: Option<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub depends: Vec<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub recommends: Vec<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub suggests: Vec<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub extra_lines: Vec<String>,
}

impl Default for Debcargo {
    fn default() -> Self {
        Debcargo {
            overlay: Some(Path::new(".").to_path_buf()),
            uploaders: Vec::default(),
            semver_suffix: false,
            bin: true,
            bin_name: None,
            source: None,
            allow_prerelease_deps: false,
            excludes: Vec::default(),
            whitelist: Vec::default(),
            summary: None,
            packages: BTreeMap::default(),
        }
    }
}

impl Debcargo {
    pub fn read<P: AsRef<Path>>(path: P) -> Result<Self> {
        let mut f = File::open(path)?;
        let mut s = String::new();
        f.read_to_string(&mut s)?;
        Ok(toml::from_str(&s)?)
    }

    pub fn write<P: AsRef<Path>>(&self, path: P) -> Result<()> {
        let s = toml::to_string(self)?;
        Ok(::std::fs::write(path, s)?)
    }

    pub fn fixup(
        &mut self,
        suite: &str,
        package_name: &str,
        config: &config::Config,
    ) {
        self.overlay = Some(Path::new(".").to_path_buf());
        let vcs_git = Some(
            config
                .vcs_git_pattern
                .replace("{packagename}", package_name),
        );
        let vcs_browser = Some(
            config
                .vcs_browser_pattern
                .replace("{packagename}", package_name),
        );
        let suite = config.suites.get(suite).unwrap();
        let policy = Some(suite.standards_version.to_string());

        for uploader in &suite.uploaders {
            if !self.uploaders.contains(uploader) {
                self.uploaders.push(uploader.to_string());
            }
        }

        let section = Some(config.default_section.to_string());

        match self.source {
            Some(ref mut source) => {
                if source.vcs_git.is_none() {
                    source.vcs_git = vcs_git;
                }
                if source.vcs_browser.is_none() {
                    source.vcs_browser = vcs_browser;
                }
                source.policy = policy;
                if source.section.is_none() {
                    source.section = section;
                }
            }
            None => {
                self.source = Some(Source {
                    vcs_git,
                    vcs_browser,
                    policy,
                    section,
                    build_depends: None,
                    homepage: None,
                });
            }
        }
    }
}
