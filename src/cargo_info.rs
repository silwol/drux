type DateTime = ::chrono::DateTime<::chrono::offset::FixedOffset>;

use semver;
use std::collections::BTreeMap;
use Result;

fn is_same_semver(a: &semver::Version, b: &semver::Version) -> bool {
    match (a.major, b.major) {
        (0u64, 0u64) => match (a.minor, b.minor) {
            (0u64, 0u64) => a.patch == b.patch,
            (a_minor, b_minor) => a_minor == b_minor,
        },
        (a_major, b_major) => a_major == b_major,
    }
}

pub fn extract_highest_version<'a>(
    current: &'a str,
    available: &'a str,
    allow_semver_upgrades: bool,
    allow_prerelease: bool,
) -> Result<&'a str> {
    let c = semver::Version::parse(current)?;
    let a = semver::Version::parse(available)?;

    if a <= c {
        Ok(current)
    } else if is_same_semver(&a, &c) {
        Ok(available)
    } else if allow_semver_upgrades {
        if a.is_prerelease() && allow_prerelease {
            Ok(available)
        } else if !a.is_prerelease() {
            Ok(available)
        } else {
            Ok(current)
        }
    } else {
        Ok(current)
    }
}

#[cfg(test)]
mod tests {
    use super::extract_highest_version as e;
    #[test]
    fn highest_version() {
        let allow_pre = true;
        let deny_pre = false;
        let allow_sem = true;
        let deny_sem = false;
        assert_eq!(
            e("1.0.0", "1.0.0", deny_sem, deny_pre).unwrap(),
            "1.0.0"
        );

        assert_eq!(
            e("1.0.0", "2.0.0", deny_sem, deny_pre).unwrap(),
            "1.0.0"
        );
        assert_eq!(
            e("1.0.0", "2.0.0", allow_sem, deny_pre).unwrap(),
            "2.0.0"
        );

        assert_eq!(
            e("0.1.0", "0.2.0", deny_sem, deny_pre).unwrap(),
            "0.1.0"
        );
        assert_eq!(
            e("0.1.0", "0.2.0", allow_sem, deny_pre).unwrap(),
            "0.2.0"
        );

        assert_eq!(
            e("1.0.0-alpha.3", "1.0.0-alpha.5", deny_sem, deny_pre)
                .unwrap(),
            "1.0.0-alpha.5"
        );
        assert_eq!(
            e("1.0.0-alpha.5", "1.0.0-alpha.3", allow_sem, allow_pre)
                .unwrap(),
            "1.0.0-alpha.5"
        );
        assert_eq!(
            e("1.0.0", "1.0.0-alpha.5", deny_sem, allow_pre).unwrap(),
            "1.0.0"
        );

        assert_eq!(
            e("1.0.0-alpha.3", "2.0.0-beta.5", deny_sem, deny_pre)
                .unwrap(),
            "1.0.0-alpha.3"
        );
        assert_eq!(
            e("1.0.0", "2.0.0-alpha.5", allow_sem, deny_pre).unwrap(),
            "1.0.0"
        );
        assert_eq!(
            e("1.0.0", "2.0.0-alpha.5", allow_sem, allow_pre).unwrap(),
            "2.0.0-alpha.5"
        );
        assert_eq!(
            e("1.0.0", "2.0.0-alpha.5", deny_sem, allow_pre).unwrap(),
            "1.0.0"
        );
    }
}

pub trait HasVersionString {
    fn version_string<'a>(&'a self) -> &'a str;
}

impl HasVersionString for str {
    fn version_string<'a>(&'a self) -> &'a str {
        self
    }
}

impl HasVersionString for Version {
    fn version_string<'a>(&'a self) -> &'a str {
        &self.num
    }
}

pub trait UpdateCandidate<'a> {
    fn update_candidate(
        &'a mut self,
        current_version: &'a str,
        allow_semver_upgrades: bool,
        allow_prerelease: bool,
    ) -> Result<&'a str>;
}

impl<'a, I: 'a + Iterator<Item = &'a T>, T: 'a + HasVersionString>
    UpdateCandidate<'a> for I
{
    fn update_candidate(
        &'a mut self,
        current_version: &'a str,
        allow_semver_upgrades: bool,
        allow_prerelease: bool,
    ) -> Result<&'a str> {
        let mut latest = current_version;
        while let Some(item) = self.next() {
            latest = extract_highest_version(
                latest,
                item.version_string(),
                allow_semver_upgrades,
                allow_prerelease,
            )?;
        }
        /*
        for item in self {
            latest = extract_highest_version(
                latest,
                item.version_string(),
                allow_semver_upgrades,
                allow_prerelease,
            )?;
        }
        */
        Ok(latest)
    }
}

/*
impl<T: HasVersionString> UpdateCandidate for Vec<T> {
    fn update_candidate<'a>(
        &'a self,
        current_version: &'a str,
        allow_semver_upgrades: bool,
        allow_prerelease: bool,
    ) -> Result<&'a str> {
        let mut latest = current_version;
        for item in self {
            latest = extract_highest_version(
                latest,
                item.version_string(),
                allow_semver_upgrades,
                allow_prerelease,
            )?;
        }
        Ok(latest)
    }
}
*/

pub trait IsVersionAvailable {
    fn is_version_available(&self, version: &str) -> bool;
}

impl<T: HasVersionString> IsVersionAvailable for Vec<T> {
    fn is_version_available(&self, version: &str) -> bool {
        self.iter()
            .find(|s| s.version_string() == version)
            .is_some()
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Info {
    #[serde(rename = "crate")]
    pub crate_: Crate,
    pub versions: Vec<Version>,
    pub keywords: Vec<Keyword>,
    pub categories: Vec<Category>,
}

impl Info {
    pub fn fetch(crate_name: &str) -> Result<Self> {
        let url =
            format!("https://crates.io/api/v1/crates/{}", crate_name);
        Ok(reqwest::get(&url)?.json()?)
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Crate {
    pub keywords: Vec<String>,
    pub categories: Vec<String>,
    pub versions: Vec<usize>,
    pub badges: Vec<Badge>,
    pub created_at: DateTime,
    pub downloads: usize,
    pub recent_downloads: usize,
    pub max_version: String,
    pub description: String,
    pub homepage: Option<String>,
    pub documentation: Option<String>,
    pub repository: Option<String>,
    pub links: CrateLinks,
    pub exact_match: bool,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Version {
    pub id: usize,
    #[serde(rename = "crate")]
    pub crate_: String,
    pub num: String,
    pub dl_path: String,
    pub readme_path: String,
    pub updated_at: DateTime,
    pub created_at: DateTime,
    pub downloads: usize,
    pub features: BTreeMap<String, Vec<String>>,
    pub yanked: bool,
    pub license: String,
    pub links: VersionLinks,
    pub crate_size: Option<usize>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Keyword {
    pub id: String,
    pub keyword: String,
    pub created_at: DateTime,
    pub crates_cnt: usize,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Category {
    pub id: String,
    pub category: String,
    pub slug: String,
    pub description: String,
    pub created_at: DateTime,
    pub crates_cnt: usize,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct AppveyorBadge {
    pub service: Option<String>,
    pub repository: String,
    pub project_name: Option<String>,
    pub id: Option<String>,
    pub branche: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct AzureDevopsBadge {
    pub build: String,
    pub pipeline: String,
    pub project: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CodecovBadge {
    pub repository: String,
    pub branch: Option<String>,
    pub service: Option<CodecovService>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum CodecovService {
    Github,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct GitlabBadge {
    pub repository: String,
    pub branch: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct TravisCiBadge {
    pub repository: String,
    pub branch: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CoverallsBadge {
    pub repository: String,
    pub branch: Option<String>,
    pub service: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct MaintenanceBadge {
    pub status: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct IsItMaintainedIssueResolutionBadge {
    pub repository: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct IsItMaintainedOpenIssuesBadge {
    pub repository: String,
}

#[derive(Debug, Deserialize)]
#[serde(
    tag = "badge_type",
    content = "attributes",
    rename_all = "kebab-case"
)]
pub enum Badge {
    Appveyor(AppveyorBadge),
    AzureDevops(AzureDevopsBadge),
    Codecov(CodecovBadge),
    Coveralls(CoverallsBadge),
    Gitlab(GitlabBadge),
    IsItMaintainedIssueResolution(IsItMaintainedIssueResolutionBadge),
    IsItMaintainedOpenIssues(IsItMaintainedOpenIssuesBadge),
    Maintenance(MaintenanceBadge),
    TravisCi(TravisCiBadge),
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct CrateLinks {
    version_downloads: String,
    versions: Option<String>,
    owners: String,
    owner_team: String,
    owner_user: String,
    reverse_dependencies: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct VersionLinks {
    dependencies: String,
    version_downloads: String,
    authors: String,
}
