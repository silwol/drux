use std::collections::BTreeMap;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use toml;

use Result;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Package {
    pub name: String,
    pub version: String,
    pub authors: Vec<String>,
    pub license: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(untagged, rename_all = "snake_case")]
pub enum Dependency {
    Version(String),
    Details {
        #[serde(default = "Default::default")]
        optional: bool,
        version: Option<String>,
        git: Option<String>,
        path: Option<String>,
    },
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "snake_case")]
pub struct Manifest {
    pub package: Package,
    #[serde(
        default = "Default::default",
        skip_serializing_if = "BTreeMap::is_empty"
    )]
    pub dependencies: BTreeMap<String, Dependency>,
    #[serde(
        default = "Default::default",
        skip_serializing_if = "BTreeMap::is_empty"
    )]
    pub dev_dependencies: BTreeMap<String, Dependency>,
}

impl Manifest {
    pub fn read<P: AsRef<Path>>(path: P) -> Result<Self> {
        let mut f = File::open(path)?;
        let mut s = String::new();
        f.read_to_string(&mut s)?;
        Ok(toml::from_str(&s)?)
    }
}
