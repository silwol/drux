#[macro_use]
extern crate failure;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate log;

#[macro_use]
extern crate serde_derive;

extern crate chrono;
extern crate directories;
extern crate env_logger;
extern crate fs_extra;
extern crate git2;
extern crate glob;
extern crate regex;
extern crate semver;
extern crate serde;
extern crate serde_json;
extern crate structopt;
extern crate tempfile;
extern crate toml;
extern crate uuid;

mod cargo_info;
mod cargo_toml;
mod config;
mod debcargo_toml;

use directories::ProjectDirs;
use failure::err_msg;
use git2::{BranchType, Repository};
use glob::glob;
use regex::Regex;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::Command;
use structopt::StructOpt;
use tempfile::Builder;
use uuid::Uuid;

type Result<T> = std::result::Result<T, failure::Error>;

#[derive(StructOpt, Debug)]
struct StatusAction {
    /// Allow prerelease (alpha, beta) versions.
    #[structopt(long = "allow-prerelease")]
    allow_prerelease: bool,
}

impl StatusAction {
    fn run(&self) -> Result<()> {
        let Environment {
            current_git_branch: _,
            git_branches: _,
            upstream_git_tags: _,
            debcargo_toml,
            manifest,
        } = introspect_environment(true)?;

        let cargo_info = cargo_info::Info::fetch(&manifest.package.name)?;

        check_version_upgrade(
            &manifest,
            &cargo_info,
            &debcargo_toml,
            self.allow_prerelease,
        )?;

        Ok(())
    }
}

fn check_version_upgrade<'a>(
    manifest: &'a cargo_toml::Manifest,
    cargo_info: &'a cargo_info::Info,
    debcargo_toml: &debcargo_toml::Debcargo,
    allow_prerelease: bool,
) -> Result<Option<String>> {
    let packaged_version_string = &manifest.package.version;

    use cargo_info::UpdateCandidate;
    let mut iter = cargo_info.versions.iter().filter(|v| !v.yanked);
    let available_version_string = iter
        .update_candidate(
            &packaged_version_string,
            !debcargo_toml.semver_suffix,
            allow_prerelease,
        )?
        .to_string();

    let packaged_version = semver::Version::parse(packaged_version_string);
    let available_version =
        semver::Version::parse(&available_version_string);

    if packaged_version == available_version {
        info!(
            "Package version {} is up-to-date.",
            packaged_version_string
        );
        return Ok(None);
    }

    if available_version < packaged_version {
        bail!(
            "Packaged version {} is newer than latest \
             available version {}",
            packaged_version_string,
            available_version_string
        );
    }

    error!("Packaged version: {}", packaged_version_string);
    error!("Newer version available: {}", available_version_string);

    Ok(Some(available_version_string))
}

fn fixup_package(
    dir: &Path,
    config: &config::Config,
    suite: &str,
) -> Result<()> {
    let suite = config.suites.get(suite).unwrap();
    {
        let p = dir.join("debian").join("control");
        let s = std::fs::read_to_string(&p)?;
        let s = {
            let re = Regex::new(
                r"(?m:^Maintainer: Debian Rust Maintainers.*$)",
            )
            .unwrap();
            let replacer = format!(
                "Maintainer: {} <{}>",
                config.maintainer_name, config.maintainer_email
            );
            re.replace_all(&s, replacer.as_str())
        };
        let s = {
            let re = Regex::new(r"(?m:^Uploaders: .*$\n)").unwrap();
            re.replace_all(&s, "")
        };
        let s = {
            let re = Regex::new("debhelper \\(>= .*\\)").unwrap();
            let replacer =
                format!("debhelper (>= {})", suite.min_debhelper_version);
            re.replace_all(&s, replacer.as_str())
        };
        let s = {
            let re = Regex::new("dh-cargo \\(>= .*\\)").unwrap();
            let replacer =
                format!("dh-cargo (>= {})", suite.min_dh_cargo_version);
            re.replace_all(&s, replacer.as_str())
        };
        std::fs::write(&p, s.to_string())?;
    }
    {
        let p = dir.join("debian").join("compat");
        let s = format!("{}\n", suite.compat);
        std::fs::write(p, &s)?;
    }
    if !suite.add_lintian_overrides.is_empty()
        || !suite.remove_lintian_overrides.is_empty()
    {
        let p =
            dir.join("debian").join("source").join("lintian-overrides");
        {
            use std::fs::File;
            let mut overrides: Vec<String> = if p.exists() {
                use std::io::{BufRead, BufReader};

                let f = File::open(&p).unwrap();
                let r = BufReader::new(&f);
                r.lines()
                    .map(|l| l.unwrap().to_string())
                    .filter(|l| {
                        !suite.remove_lintian_overrides.contains(l)
                    })
                    .collect()
            } else {
                Vec::new()
            };
            for add in suite.add_lintian_overrides.iter() {
                if !overrides.contains(add) {
                    overrides.push(add.to_string());
                }
            }
            if !overrides.is_empty() {
                let mut f = File::create(&p).unwrap();
                writeln!(f, "{}", overrides.join("\n")).unwrap();
            }
        }
    }
    Ok(())
}

fn rsync_from_temp_dir<P: AsRef<Path>, Q: AsRef<Path>>(
    from: P,
    to: Q,
) -> Result<()> {
    let from = from.as_ref().join(".");
    let to = to.as_ref().join(".");
    Command::new("rsync")
        .arg("-a")
        .arg("--exclude=debian/changelog")
        .arg("--exclude=debian/copyright")
        .arg("--exclude=debian/*.hint")
        .arg("--exclude=.pc")
        .arg(&from)
        .arg(&to)
        .run_checked_and_logged()
        .map(|_| ())
}

#[derive(StructOpt, Debug)]
struct RebuildAction {}

impl RebuildAction {
    fn run<P: AsRef<Path>>(
        &self,
        config: &config::Config,
        suite: &str,
        cache_dir: P,
        keep_tempdir: bool,
    ) -> Result<()> {
        let Environment {
            current_git_branch: _,
            git_branches: _,
            upstream_git_tags: _,
            debcargo_toml,
            manifest,
        } = introspect_environment(true)?;

        let version = &manifest.package.version;

        let temp_dir = Builder::new()
            .prefix(&manifest.package.name)
            .tempdir_in(cache_dir.as_ref())?;
        let package_name =
            manifest.package.name.replace("_", "-").to_lowercase();

        let dir = if keep_tempdir {
            temp_dir.into_path()
        } else {
            temp_dir.path().to_path_buf()
        };
        info!("Building package in temporary path {:?}", dir);

        let debcargo_template = copy_to_temp_dir(&dir)?;

        let mut debcargo_toml = debcargo_toml;
        debcargo_toml.fixup(suite, &package_name, config);
        debcargo_toml.write(&debcargo_template)?;

        let package_dir = dir.join("package");

        Command::new("debcargo")
            .current_dir(&dir)
            .env("DEBFULLNAME", &config.maintainer_name)
            .env("DEBEMAIL", &config.maintainer_email)
            .arg("package")
            .arg("--config")
            .arg(debcargo_template)
            .arg("--directory")
            .arg(&package_dir)
            .arg(manifest.package.name)
            .arg(version)
            .run_checked_and_logged()?;

        fixup_package(&package_dir, config, suite)?;
        rsync_from_temp_dir(&package_dir, ".")?;

        Ok(())
    }
}

fn copy_to_temp_dir<P: AsRef<Path>>(dir: P) -> Result<PathBuf> {
    let template_dir = dir.as_ref().join("debcargo-conf");
    std::fs::create_dir_all(template_dir.join("debian"))?;

    let debcargo_template = {
        let p = "debian/debcargo.toml";

        let debcargo_template = template_dir.join(p);
        std::fs::copy(p, &debcargo_template)?;

        debcargo_template
    };

    {
        let p = "debian/rules";
        std::fs::copy(p, &template_dir.join(p))?;
    };

    {
        let p = "debian/patches";
        if Path::new(p).is_dir() {
            let d = template_dir.join("debian");
            let mut options = fs_extra::dir::CopyOptions::new();
            options.overwrite = true;
            fs_extra::dir::copy(p, d, &options)?;
        }
    }

    {
        let dir = Path::new("debian").join("source");
        let p = dir.join("lintian-overrides");
        if p.is_file() {
            let d = template_dir.join(&dir);
            std::fs::create_dir_all(&d)?;
            let d = template_dir.join(&p);
            std::fs::copy(&p, &d)?;
        }
    }

    // TODO: copy other required files in the debian directory

    Ok(debcargo_template)
}

#[derive(StructOpt, Debug)]
struct PackageAction {
    /// The crate name.
    package_name: String,

    /// Use this version.
    /// If not present, the latest non-pre-release version is used.
    version: Option<String>,

    /// Allow prerelease (alpha, beta) versions of dependencies.
    #[structopt(long = "allow-prerelease-deps")]
    allow_prerelease_deps: bool,

    /// Set semver suffix.
    #[structopt(long = "with-semver-suffix")]
    with_semver_suffix: bool,

    /// Set an additional debcargo.toml as input.
    #[structopt(long = "base-config")]
    base_config: Option<PathBuf>,

    /// Add a copyright file right at the start.
    #[structopt(long = "copyright-file")]
    copyright_file: Option<PathBuf>,
}

impl PackageAction {
    fn run<P: AsRef<Path>>(
        &self,
        config: &config::Config,
        suite: &str,
        cache_dir: P,
        keep_tempdir: bool,
    ) -> Result<()> {
        let cargo_info = cargo_info::Info::fetch(&self.package_name)?;

        let version = if let Some(version) = &self.version {
            info!("Version {:?} requested by parameter.", version);
            info!("Allowing prerelease versions.");
            use cargo_info::IsVersionAvailable;
            if !cargo_info.versions.is_version_available(version) {
                bail!(
                    "Verison {:?} is not available for crate {:?}",
                    self.package_name,
                    version
                );
            }
            version.to_string()
        } else {
            cargo_info.crate_.max_version.to_string()
        };

        let semver_version = semver::Version::parse(&version)?;

        let mut debcargo_toml = if let Some(ref path) = self.base_config {
            debcargo_toml::Debcargo::read(&path)?
        } else {
            debcargo_toml::Debcargo::default()
        };
        debcargo_toml.allow_prerelease_deps = self.allow_prerelease_deps;
        debcargo_toml.semver_suffix = self.with_semver_suffix;
        let package_name =
            self.package_name.replace("_", "-").to_lowercase();
        debcargo_toml.fixup(suite, &package_name, config);

        let temp_dir = Builder::new()
            .prefix(&package_name)
            .tempdir_in(cache_dir.as_ref())?;
        let dir = if keep_tempdir {
            temp_dir.into_path()
        } else {
            temp_dir.path().to_path_buf()
        };
        let template_dir = dir.join("debcargo-conf");
        std::fs::create_dir_all(template_dir.join("debian"))?;

        let debcargo_template = {
            let p = "debian/debcargo.toml";

            let debcargo_template = template_dir.join(p);
            debcargo_toml.write(&debcargo_template)?;

            debcargo_template
        };

        let package_dir = dir.join("package");

        Command::new("debcargo")
            .current_dir(&dir)
            .env("DEBFULLNAME", &config.maintainer_name)
            .env("DEBEMAIL", &config.maintainer_email)
            .arg("package")
            .arg("--config")
            .arg(debcargo_template)
            .arg("--directory")
            .arg(&package_dir)
            .arg(&self.package_name)
            .arg(&version)
            .run_checked_and_logged()?;

        fixup_package(&package_dir, config, suite)?;

        let significant_version = match (
            semver_version.major,
            semver_version.minor,
            semver_version.patch,
        ) {
            (a, b, c) if a == 0 && b == 0 => format!("{}.{}.{}", a, b, c),
            (a, b, _) if a == 0 => format!("{}.{}", a, b),
            (a, _, _) => format!("{}", a),
        };

        let orig_tar_gz = if self.with_semver_suffix {
            dir.join(format!(
                "rust-{}-{}_{}.orig.tar.gz",
                package_name, significant_version, version
            ))
        } else {
            dir.join(format!(
                "rust-{}_{}.orig.tar.gz",
                package_name, version
            ))
        };

        let package_full_name = if self.with_semver_suffix {
            format!("rust-{}-{}", package_name, significant_version)
        } else {
            format!("rust-{}", package_name)
        };

        std::fs::create_dir_all(&package_full_name)?;
        rsync_from_temp_dir(&package_dir, &package_full_name)?;

        if let Some(ref path) = self.copyright_file {
            let dest_path =
                Path::new(&package_full_name).join("debian/copyright");
            std::fs::copy(&path, &dest_path)?;
        }

        {
            let version =
                format!("{}-0{}1", version, config.inhouse_appendix);
            let message = format!("Package {} {}", package_name, version);
            Command::new("dch")
                .current_dir(&package_full_name)
                .env("DEBFULLNAME", &config.maintainer_name)
                .env("DEBEMAIL", &config.maintainer_email)
                .arg("--create")
                .arg("--package")
                .arg(&package_full_name)
                .arg("--newversion")
                .arg(&version)
                .arg("--distribution")
                .arg(suite)
                .arg("--force-distribution")
                .arg(message)
                .run_checked_and_logged()?;
        }

        Command::new("git")
            .current_dir(&package_full_name)
            .arg("init")
            .run_checked_and_logged()?;
        let vcs_remote = config
            .git_remote_pattern
            .replace("{packagename}", &package_name);
        Command::new("git")
            .current_dir(&package_full_name)
            .arg("remote")
            .arg("add")
            .arg("origin")
            .arg(vcs_remote)
            .run_checked_and_logged()?;
        Command::new("gbp")
            .current_dir(&package_full_name)
            .arg("import-orig")
            .arg("--no-interactive")
            .arg("--no-sign-tags")
            .arg("--pristine-tar")
            .arg("--upstream-version")
            .arg(version)
            .arg(orig_tar_gz)
            .run_checked_and_logged()?;
        Command::new("git")
            .current_dir(&package_full_name)
            .arg("add")
            .arg(".")
            .run_checked_and_logged()?;
        Command::new("git")
            .current_dir(&package_full_name)
            .arg("commit")
            .arg("-m")
            .arg("Initial import")
            .run_checked_and_logged()?;
        let current_git_branch = format!("{}/repo/druximport", suite);
        Command::new("git")
            .current_dir(&package_full_name)
            .arg("checkout")
            .arg("-b")
            .arg(&current_git_branch)
            .run_checked_and_logged()?;

        info!("");
        error!("The initial packaging is finished.");
        error!("Please check the git log, and push to the server for release:");
        error!("  cd {}", package_full_name);
        error!("  git push origin --tags");
        error!("  git push --set-upstream origin upstream");
        error!("  git push --set-upstream origin pristine-tar");
        error!("  git push --set-upstream origin {}", current_git_branch);

        Ok(())
    }
}

#[derive(StructOpt, Debug)]
struct UpdateAction {
    /// Add this option in order to specify an exact version.
    version: Option<String>,

    /// Allow prerelease (alpha, beta) versions.
    #[structopt(long = "allow-prerelease")]
    allow_prerelease: bool,

    /// Allow prerelease (alpha, beta) versions of dependencies.
    #[structopt(long = "allow-prerelease-deps")]
    allow_prerelease_deps: bool,

    /// Ignore the git branch
    #[structopt(long = "git-ignore-branch")]
    git_ignore_branch: bool,

    /// Don't enforce repo branch
    #[structopt(long = "git-dont-enforce-repo-branch")]
    git_dont_enforce_repo_branch: bool,
}

impl UpdateAction {
    fn run<P: AsRef<Path>>(
        &self,
        config: &config::Config,
        suite: &str,
        cache_dir: P,
        keep_tempdir: bool,
    ) -> Result<()> {
        let Environment {
            current_git_branch,
            git_branches,
            upstream_git_tags,
            debcargo_toml,
            manifest,
        } = introspect_environment(true)?;

        let cargo_info = cargo_info::Info::fetch(&manifest.package.name)?;

        let version = if let Some(new_version) = &self.version {
            info!("Version {:?} requested by parameter.", new_version);
            info!("Allowing semver upgrade and prerelease versions.");
            use cargo_info::IsVersionAvailable;
            if !cargo_info.versions.is_version_available(new_version) {
                bail!(
                    "Version {:?} is not available for crate {:?}",
                    manifest.package.name,
                    new_version
                );
            }

            let highest_version = cargo_info::extract_highest_version(
                &manifest.package.version,
                new_version,
                true,
                true,
            )?;
            if highest_version != new_version {
                bail!(
                    "Downgrade from version {:?} to {:?} impossible",
                    manifest.package.version,
                    new_version
                );
            }

            Some(new_version.to_string())
        } else if let Some(new_version) = check_version_upgrade(
            &manifest,
            &cargo_info,
            &debcargo_toml,
            self.allow_prerelease,
        )? {
            Some(new_version.to_string())
        } else {
            None
        };

        if let Some(new_version_string) = version {
            let new_version = semver::Version::parse(&new_version_string)?;

            let master_pattern = Regex::new("^[a-z0-9]*/master$")?;
            let debian_pattern = Regex::new("^[a-z0-9]*/debian$")?;
            let repo_pattern = Regex::new("^[a-z0-9]*/repo/[-_a-z0-9]*$")?;

            if master_pattern.is_match(&current_git_branch) {
                warn!("Current git branch {:?}.", current_git_branch);
                warn!("It looks like this is a master branch.");
                warn!(
                    "You should better work on a repo branch, and merge \
                     to {:?} when finished",
                    current_git_branch
                );
                warn!("  git checkout -b {}/repo/<feature>", suite);
                if !self.git_dont_enforce_repo_branch {
                    warn!(
                        "You can override this by passing \
                         the `--git-dont-enforce-repo-branch` argument"
                    );
                    bail!("Not using master branch")
                }
            } else if debian_pattern.is_match(&current_git_branch) {
                warn!("Current git branch {:?}.", current_git_branch);
                warn!("It looks like this is a debian branch.");
                warn!(
                    "You should better work on a repo branch, and merge \
                     to {:?} when finished",
                    current_git_branch
                );
                warn!("  git checkout -b {}/repo/<feature>", suite);
                if !self.git_dont_enforce_repo_branch {
                    warn!(
                        "You can override this by passing \
                         the `--git-dont-enforce-repo-branch` argument"
                    );
                    bail!("Not using debian branch")
                }
            } else if !repo_pattern.is_match(&current_git_branch) {
                if !self.git_ignore_branch {
                    bail!(
                        "You must be working on either a <suite>/master, \
                         <suite>/debian or <suite>/repo/<feature> branch."
                    )
                };
            }

            let temp_dir = Builder::new()
                .prefix(&manifest.package.name)
                .tempdir_in(cache_dir.as_ref())?;
            let dir = if keep_tempdir {
                temp_dir.into_path()
            } else {
                temp_dir.path().to_path_buf()
            };
            info!("Building package in temporary path {:?}", dir);

            if !git_branches.contains(&"upstream".to_string()) {
                bail!(
                    "Git branch 'upstream' not found.\n\
                     If it exists on the remote, you can check it out \
                     with `git checkout upstream` and then switch back \
                     to this branch with `git checkout {}`.",
                    current_git_branch
                );
            }

            let debcargo_template = copy_to_temp_dir(&dir)?;

            let package_name =
                manifest.package.name.replace("_", "-").to_lowercase();
            let mut debcargo_toml = debcargo_toml;
            debcargo_toml.allow_prerelease_deps =
                self.allow_prerelease_deps;
            debcargo_toml.fixup(suite, &package_name, config);
            debcargo_toml.write(&debcargo_template)?;

            let package_dir = dir.join("package");

            // This is the most reliable way I found to update the cargo
            // package cache. We attempt to install an inexistant
            // package and discard the output and exit code.
            let uuid = Uuid::new_v4();
            info!("Updating cargo package cache");
            Command::new("cargo")
                .arg("install")
                .arg(format!(
                    "drux-package-does-not-exist-{}",
                    uuid.to_hyphenated()
                ))
                .output()
                .ok();

            Command::new("debcargo")
                .current_dir(&dir)
                .env("DEBFULLNAME", &config.maintainer_name)
                .env("DEBEMAIL", &config.maintainer_email)
                .arg("package")
                .arg("--config")
                .arg(debcargo_template)
                .arg("--directory")
                .arg(&package_dir)
                .arg(manifest.package.name)
                .arg(new_version_string)
                .run_checked_and_logged()?;

            let DebianOrigPackageVersionInformation {
                _name,
                version: deb_version,
            } = import_orig(
                dir,
                &new_version,
                &current_git_branch,
                &upstream_git_tags,
            )?;

            fixup_package(&package_dir, config, &config.default_suite)?;
            rsync_from_temp_dir(&package_dir, ".")?;

            Command::new("git")
                .arg("add")
                .arg(".")
                .run_checked_and_logged()?;

            Command::new("git")
                .arg("commit")
                .arg("-m")
                .arg(format!(
                    "Update packaging to match version {}",
                    deb_version
                ))
                .run_checked_and_logged()?;

            let version =
                format!("{}-0{}1", deb_version, config.inhouse_appendix);

            Command::new("gbp")
                .arg("dch")
                .arg("--git-author")
                .arg("-a")
                .arg("-R")
                .arg("--id-length=7")
                .arg("--ignore-branch")
                .arg("--spawn-editor=never")
                .arg("--distribution")
                .arg(suite)
                .arg("--force-distribution")
                .arg(format!("--debian-branch={}", current_git_branch))
                .arg("--new-version")
                .arg(&version)
                .run_checked_and_logged()?;

            Command::new("git")
                .arg("commit")
                .arg("debian/changelog")
                .arg("-m")
                .arg(format!("Release {}.", version))
                .run_checked_and_logged()?;

            info!("");
            error!("The update is finished.");
            error!("Please check the git log, and push to the server for release:");
            error!("  git push origin --tags");
            error!("  git push --set-upstream origin upstream");
            error!("  git push --set-upstream origin pristine-tar");
            error!(
                "  git push --set-upstream origin {}",
                current_git_branch
            );

            Ok(())
        } else {
            Ok(())
        }
    }
}

#[derive(StructOpt, Debug)]
struct CreateDebcargoTomlAction {}

impl CreateDebcargoTomlAction {
    fn run(&self, config: &config::Config, suite: &str) -> Result<()> {
        let Environment {
            current_git_branch: _,
            git_branches: _,
            upstream_git_tags: _,
            debcargo_toml: _,
            manifest,
        } = introspect_environment(false)?;

        let mut debcargo_toml = debcargo_toml::Debcargo::default();
        let package_name =
            manifest.package.name.replace("_", "-").to_lowercase();
        debcargo_toml.fixup(suite, &package_name, config);
        debcargo_toml.write("debian/debcargo.toml")?;

        Ok(())
    }
}

#[derive(StructOpt, Debug)]
struct PrepareCopyrightAction {}

impl PrepareCopyrightAction {
    fn run<P: AsRef<Path>>(
        &self,
        config: &config::Config,
        suite: &str,
        cache_dir: P,
        keep_tempdir: bool,
    ) -> Result<()> {
        let Environment {
            debcargo_toml,
            manifest,
            ..
        } = introspect_environment(true)?;

        let version = manifest.package.version;

        let destination = Path::new("debian/copyright");
        if destination.exists() {
            bail!("Destination file 'debian/copyright' already exists");
        }

        let temp_dir = Builder::new()
            .prefix(&manifest.package.name)
            .tempdir_in(cache_dir.as_ref())?;
        let dir = if keep_tempdir {
            temp_dir.into_path()
        } else {
            temp_dir.path().to_path_buf()
        };
        info!("Building package in temporary path {:?}", dir);

        let debcargo_template = copy_to_temp_dir(&dir)?;

        let package_name =
            manifest.package.name.replace("_", "-").to_lowercase();
        let mut debcargo_toml = debcargo_toml;
        debcargo_toml.fixup(suite, &package_name, config);
        debcargo_toml.write(&debcargo_template)?;

        let package_dir = dir.join("package");

        // This is the most reliable way I found to update the cargo
        // package cache. We attempt to install an inexistant
        // package and discard the output and exit code.
        let uuid = Uuid::new_v4();
        info!("Updating cargo package cache");
        Command::new("cargo")
            .arg("install")
            .arg(format!(
                "drux-package-does-not-exist-{}",
                uuid.to_hyphenated()
            ))
            .output()
            .ok();

        Command::new("debcargo")
            .current_dir(&dir)
            .env("DEBFULLNAME", &config.maintainer_name)
            .env("DEBEMAIL", &config.maintainer_email)
            .arg("package")
            .arg("--config")
            .arg(debcargo_template)
            .arg("--directory")
            .arg(&package_dir)
            .arg(manifest.package.name)
            .arg(version)
            .run_checked_and_logged()?;

        let source = package_dir.join("debian").join("copyright");
        info!("Copying {:?} to {:?}", source, destination);
        std::fs::copy(source, destination)?;

        info!("");
        error!("Copyright file template placed under debian/copyright");
        error!(
            "Please correct all FIXME markers, and then commit it in git"
        );

        Ok(())
    }
}

struct DebianOrigPackageVersionInformation {
    _name: String,
    version: String,
}

trait RunCheckedAndLogged {
    fn run_checked_and_logged(&mut self) -> Result<std::process::Output> {
        self.run_checked_and_logged_with_msg("")
    }

    fn run_checked_and_logged_with_msg(
        &mut self,
        msg: &str,
    ) -> Result<std::process::Output>;
}

impl RunCheckedAndLogged for std::process::Command {
    fn run_checked_and_logged_with_msg(
        &mut self,
        msg: &str,
    ) -> Result<std::process::Output> {
        info!("Running {:?}", self);
        let o = self.output()?;
        let msg = if msg.is_empty() {
            format!("")
        } else {
            format!("\n{}", msg)
        };
        match (o.status.success(), o.status.code()) {
            (false, None) => bail!(
                "Failed to run, exit code unknown{}\n\n{}",
                msg,
                String::from_utf8(o.stderr).unwrap_or("".to_string())
            ),
            (false, Some(code)) => bail!(
                "Failed to run, exited with code {}{}\n\n{}",
                code,
                msg,
                String::from_utf8(o.stderr).unwrap_or("".to_string())
            ),
            _ => Ok(o),
        }
    }
}

fn import_orig<P: AsRef<Path>>(
    dir_path: P,
    version: &semver::Version,
    current_git_branch: &str,
    upstream_git_tags: &Vec<String>,
) -> Result<DebianOrigPackageVersionInformation> {
    let s = dir_path.as_ref().to_str().unwrap();
    let pattern = format!(
        "{}/*_{}.{}.{}*.orig.tar.gz",
        s, version.major, version.minor, version.patch
    );
    let re = Regex::new("^(.*)_(.*)\\.orig\\.tar\\.gz$").unwrap();
    for entry in glob(&pattern)? {
        let path = entry?;
        info!("Found orig archive {:?}", path);
        let filename = path.file_name().unwrap().to_str().unwrap();
        let items = re.captures(filename).unwrap();
        let _name = items[1].to_string();
        let version = items[2].to_string();

        let upstream_git_tag = format!("upstream/{}", version);
        if upstream_git_tags.contains(&upstream_git_tag) {
            bail!(
                "Git tag {:?} already exists, cannot import",
                upstream_git_tag
            );
        }

        info!("Importing with gbp import-orig");
        Command::new("gbp")
            .arg("import-orig")
            .arg("--no-sign-tags")
            .arg("--pristine-tar")
            .arg("--debian-branch")
            .arg(current_git_branch)
            .arg("--upstream-version")
            .arg(&version)
            .arg(&path)
            .run_checked_and_logged()?;
        return Ok(DebianOrigPackageVersionInformation { _name, version });
    }
    bail!("No file with pattern \"{}\" found", pattern)
}

#[derive(StructOpt, Debug)]
enum Action {
    #[structopt(
        name = "check",
        about = "Check the current status of the package"
    )]
    Status(StatusAction),
    #[structopt(name = "update", about = "Update the package")]
    Update(UpdateAction),
    #[structopt(name = "rebuild", about = "Rebuild the package")]
    Rebuild(RebuildAction),
    #[structopt(name = "package", about = "Create a package")]
    Package(PackageAction),
    #[structopt(
        name = "create-debcargo-toml",
        about = "Create a debcargo.toml file in the current package"
    )]
    CreateDebcargoToml(CreateDebcargoTomlAction),
    #[structopt(
        name = "prepare-copyright",
        about = "Prepare a template copyright file"
    )]
    PrepareCopyright(PrepareCopyrightAction),
}

struct Environment {
    current_git_branch: String,
    git_branches: Vec<String>,
    upstream_git_tags: Vec<String>,
    debcargo_toml: debcargo_toml::Debcargo,
    manifest: cargo_toml::Manifest,
}

fn introspect_environment(
    check_debcargo_toml: bool,
) -> Result<Environment> {
    Command::new("cargo")
        .arg("--version")
        .run_checked_and_logged_with_msg(
            "You can install cargo with:\n  cargo install cargo",
        )?;

    Command::new("debcargo")
        .arg("--version")
        .run_checked_and_logged_with_msg(
            "You can install debcargo with:\n  cargo install debcargo",
        )?;

    Command::new("gbp")
        .arg("--version")
        .run_checked_and_logged_with_msg(
        "You can install gbp with:\n  sudo apt install git-buildpackage",
    )?;

    Command::new("quilt")
        .arg("--version")
        .run_checked_and_logged_with_msg(
            "You can install quilt with:\n  sudo apt install quilt",
        )?;

    Command::new("rsync")
        .arg("--version")
        .run_checked_and_logged_with_msg(
            "You can install rsync with:\n  sudo apt install rsync",
        )?;

    let repo = match Repository::open(".") {
        Ok(repo) => repo,
        Err(e) => {
            bail!("Could not open git repo: {}", e);
        }
    };

    if !Path::new("Cargo.toml").exists() {
        bail!("No Cargo.toml file found.");
    }

    if !Path::new("debian").is_dir() {
        bail!("No debian directory found.");
    }

    let current_git_branch = head_branch(&repo, Some(BranchType::Local))?;
    let git_branches = branch_names(&repo, Some(BranchType::Local))?;
    let upstream_git_tags = tag_names(&repo, Some("upstream/*"))?;

    info!("Current git branch: {}", current_git_branch);

    let debcargo_toml = if check_debcargo_toml {
        info!("Reading debian/debcargo.toml file");
        let debcargo_path = Path::new("debian/debcargo.toml");
        if debcargo_path.exists() {
            debcargo_toml::Debcargo::read(&debcargo_path)?
        } else {
            warn!("Prjoect does not contain a debian/debcargo.toml file.");
            warn!("Using default debcargo config.");
            warn!("You should consider creating one:");
            warn!("  drux create-debcargo-toml");
            warn!("");
            debcargo_toml::Debcargo::default()
        }
    } else {
        debcargo_toml::Debcargo::default()
    };

    info!("Reading Cargo.toml file");
    let manifest = cargo_toml::Manifest::read("Cargo.toml")?;

    info!(
        "Project: {:?} {}",
        manifest.package.name, manifest.package.version
    );

    Ok(Environment {
        current_git_branch,
        git_branches,
        upstream_git_tags,
        debcargo_toml,
        manifest,
    })
}

fn head_branch(
    repo: &Repository,
    branch_type: Option<BranchType>,
) -> Result<String> {
    for branch in repo.branches(branch_type)? {
        let branch = branch?.0;
        if branch.is_head() {
            if let Some(name) = branch.name()? {
                return Ok(name.to_string());
            } else {
                bail!("Found unnamed HEAD branch");
            }
        }
    }
    bail!("No head branch found");
}

fn branch_names(
    repo: &Repository,
    branch_type: Option<BranchType>,
) -> Result<Vec<String>> {
    let mut names = Vec::new();
    for branch in repo.branches(branch_type)? {
        let branch = branch?.0;
        if let Some(name) = branch.name()? {
            names.push(name.to_string());
        }
    }
    Ok(names)
}

fn tag_names(
    repo: &Repository,
    pattern: Option<&str>,
) -> Result<Vec<String>> {
    Ok(repo
        .tag_names(pattern)?
        .iter()
        .filter_map(|o| o.map(|s| s.to_string()))
        .collect())
}

impl Action {
    fn run<P: AsRef<Path>>(
        &self,
        config: &config::Config,
        suite: &str,
        cache_dir: P,
        keep_tempdir: bool,
    ) -> Result<()> {
        use Action::*;
        std::fs::create_dir_all(cache_dir.as_ref())?;
        match self {
            Rebuild(a) => a.run(config, suite, cache_dir, keep_tempdir),
            Status(a) => a.run(),
            Update(a) => a.run(config, suite, cache_dir, keep_tempdir),
            Package(a) => a.run(config, suite, cache_dir, keep_tempdir),
            CreateDebcargoToml(a) => a.run(config, suite),
            PrepareCopyright(a) => {
                a.run(config, suite, cache_dir, keep_tempdir)
            }
        }
    }
}

#[derive(StructOpt)]
#[structopt(
    name = "drux",
    about = "Debian Rust-package Updater for inhouse"
)]
pub struct Options {
    /// Path to the drux `config.toml` file.
    /// Gets loaded from `~/.config/drux/config.toml` by default.
    #[structopt(long = "config")]
    config: Option<String>,

    /// Set a suite different from the default_suite in the config file.
    #[structopt(long = "suite")]
    suite: Option<String>,

    /// Keep the temporary directory.
    #[structopt(long = "keep-tempdir")]
    keep_tempdir: bool,

    #[structopt(subcommand)]
    action: Action,
}

impl Options {
    fn run(&self) -> Result<()> {
        let project_dirs = ProjectDirs::from("", "", "drux")
            .ok_or_else(|| err_msg("Couldn't load project dirs"))?;
        let config_path = if let Some(ref path) = self.config {
            Path::new(&path).to_path_buf()
        } else {
            project_dirs.config_dir().join("config.toml")
        };
        let config = config::Config::read(&config_path)?;
        config.validate()?;

        let suite = if let Some(ref suite) = self.suite {
            if !config.suites.contains_key(suite) {
                bail!("Suite {:?} not found in {:?}", suite, config_path);
            }
            suite.to_string()
        } else {
            config.default_suite.to_string()
        };

        self.action.run(
            &config,
            &suite,
            &project_dirs.cache_dir(),
            self.keep_tempdir,
        )
    }
}

fn log_format(
    formatter: &mut env_logger::fmt::Formatter,
    record: &log::Record,
) -> std::result::Result<(), std::io::Error> {
    use env_logger::fmt::Color;
    use log::Level;

    let level = record.level();
    let mut style = formatter.style();

    match level {
        Level::Trace => style.set_color(Color::White),
        Level::Debug => style.set_color(Color::Blue),
        Level::Info => style.set_color(Color::Green),
        Level::Warn => style.set_color(Color::Yellow),
        Level::Error => style.set_color(Color::Red).set_bold(true),
    };

    writeln!(formatter, "{}", style.value(&record.args()))?;

    Ok(())
}

fn main() {
    let env = env_logger::Env::default()
        .filter_or(env_logger::DEFAULT_FILTER_ENV, "warn,drux=info");
    let mut builder = env_logger::Builder::from_env(env);
    builder.format(log_format);
    builder.format_timestamp(None);
    builder.init();

    let opt = Options::from_args();

    match opt.run() {
        Ok(_) => {}
        Err(e) => {
            error!("{}", e);
            ::std::process::exit(1);
        }
    }
}
