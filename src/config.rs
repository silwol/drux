use regex::Regex;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use toml;
use Result;

pub fn vcs_regex() -> &'static Regex {
    lazy_static! {
        static ref RE: Regex =
            Regex::new("^.*(\\{packagename\\}).*$").unwrap();
    }
    &RE
}

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub default_suite: String,
    pub default_section: String,
    pub inhouse_appendix: String,
    pub maintainer_name: String,
    pub maintainer_email: String,
    pub vcs_git_pattern: String,
    pub vcs_browser_pattern: String,
    pub git_remote_pattern: String,
    pub suites: BTreeMap<String, Suite>,
}

#[derive(Serialize, Deserialize)]
pub struct Suite {
    pub standards_version: String,
    pub min_dh_cargo_version: String,
    pub min_debhelper_version: String,
    pub compat: usize,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub uploaders: Vec<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub add_lintian_overrides: Vec<String>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub remove_lintian_overrides: Vec<String>,
}

impl Suite {
    pub fn validate(&self, key: &str) -> Result<()> {
        let re = Regex::new("^[0-9]+(\\.[0-9]+){2,3}$").unwrap();
        if !re.is_match(&self.standards_version) {
            bail!(
                "suite {:?}: standards_version does not match regex {:?}",
                key,
                re
            );
        }
        Ok(())
    }
}

impl Config {
    pub fn validate(&self) -> Result<()> {
        let re = Regex::new("^[a-z]+").unwrap();
        if !re.is_match(&self.default_suite) {
            bail!("default_suite must match regex {:?}", re);
        }
        if !self.suites.contains_key(&self.default_suite) {
            bail!(
                "default_suite entry `{}` must be present in suites",
                self.default_suite
            );
        }
        if !re.is_match(&self.inhouse_appendix) {
            bail!("inhouse_appendix must match regex {:?}", re);
        }
        let section_re = Regex::new("^[a-z]+(/[a-z]+)*$").unwrap();
        if !section_re.is_match(&self.default_section) {
            bail!("section must match regex {:?}", section_re);
        }
        if !vcs_regex().is_match(&self.vcs_git_pattern) {
            bail!("vcs_git_pattern must match regex {:?}", vcs_regex());
        }
        if !vcs_regex().is_match(&self.vcs_browser_pattern) {
            bail!(
                "vcs_browser_pattern must match regex {:?}",
                vcs_regex()
            );
        }
        if !vcs_regex().is_match(&self.git_remote_pattern) {
            bail!("git_remote_pattern must match regex {:?}", vcs_regex());
        }
        for (ref k, ref v) in &self.suites {
            if !re.is_match(&k) {
                bail!("suite name {:?} does not match regex {:?}", k, re);
            }
            v.validate(&k)?;
        }
        Ok(())
    }

    pub fn read<P: AsRef<Path>>(path: P) -> Result<Self> {
        let mut f = File::open(path)?;
        let mut s = String::new();
        f.read_to_string(&mut s)?;
        Ok(toml::from_str(&s)?)
    }
}
